﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Timing : MonoBehaviour
{
    public Text Texto;
    public GameObject lose;
    public float time;
    public float currentTime = 20;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        DecreasingNumber();
    }

    public void DecreasingNumber() 
    {

        currentTime -= 1 * Time.deltaTime;
        if (currentTime >= 10 ) 
        {

            
            time=Mathf.Round(currentTime);

            Texto.text = "0:"+time;
                        
           
        } else if(currentTime <=9) {

            
            time = Mathf.Round(currentTime);

            Texto.text = "0:0"+time;

            if (time <= 5) {
                Texto.color = Color.red;
            } else {
                Texto.color = Color.white;
            }

        } else if (currentTime <= 0) 
        {
            lose.SetActive(true);
            Time.timeScale = 0;
        }
        
    }
}
