﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    enum directions
    {
        up, down, left, right
    }

    directions direcciones;

    public List<Transform> tail = new List<Transform>();

    public float frameRate = 0.2f;
    public float step = 0.16f;
    public GameObject tailPrefab;

    public GameObject refe;
    public Objetivos obj;


    public GameObject win;
    public GameObject lose;

    public GameObject letra;

    

    // Start is called before the first frame update
    void Start()
    {
        
        InvokeRepeating("Move", frameRate, frameRate);

        obj = refe.GetComponent<Objetivos>();
        obj.ObjUpdate();
        
    }
    
    void Move()
    {
        lastPos = transform.position;
        lastAng = transform.eulerAngles;

        Vector3 nextPos = Vector3.zero;
        if (direcciones == directions.up)
        {
            nextPos = Vector3.up;
            gameObject.transform.eulerAngles = new Vector3(0, 0, 0);
        } 
        else if (direcciones == directions.down)
        {
            nextPos = Vector3.down;
            gameObject.transform.eulerAngles = new Vector3(0, 0, 180);
        }
            
        else if (direcciones == directions.left)
        {
            nextPos = Vector3.left;
            gameObject.transform.eulerAngles = new Vector3(0, 0, 90);
        }
            
        else if (direcciones == directions.right)
        {
            nextPos = Vector3.right;
            gameObject.transform.eulerAngles = new Vector3(0, 0, -90);
        }
            

        nextPos *= step;
        transform.position += nextPos;
        MoveTail();
    }

    Vector3 lastPos;
    Vector3 lastAng;
    void MoveTail()
    {
        for (int i = 0; i < tail.Count; i++)
        {
            Vector3 temp = tail[i].position;
            Vector3 ang = tail[i].eulerAngles;

            tail[i].position = lastPos;
            tail[i].eulerAngles = lastAng;

            lastPos = temp;
            lastAng = ang;
            
        }
    }

    public void UpButton()
    {
        if (direcciones != directions.down)
        direcciones = directions.up;
    }
    public void DownButton()
    {
        if(direcciones != directions.up)
        direcciones = directions.down;
    }
    public void LeftButton()
    {
        if(direcciones != directions.right)
        direcciones = directions.left;
    }
    public void RightButton()
    {
        if(direcciones != directions.left)
        direcciones = directions.right;
    }

    void Inputs()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && direcciones!=directions.down)
            direcciones = directions.up;
        else if (Input.GetKeyDown(KeyCode.DownArrow) && direcciones != directions.up)
            direcciones = directions.down;
        else if (Input.GetKeyDown(KeyCode.LeftArrow) && direcciones != directions.right)
            direcciones = directions.left;
        else if (Input.GetKeyDown(KeyCode.RightArrow) && direcciones != directions.left)
            direcciones = directions.right;
    }

    // Update is called once per frame
    void Update()
    {
        Inputs();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Walls"))
        {

            //Scene scene = SceneManager.GetActiveScene();

            //SceneManager.LoadScene(scene.name);

            lose.SetActive(true);
            Time.timeScale = 0;
        }
        else if (col.CompareTag("Food"))
        {
            tail.Add(
            Instantiate(tailPrefab, tail[tail.Count - 1].position, Quaternion.identity).transform);

            col.gameObject.SetActive(false);

            obj.destroys += 1;
            obj.ObjUpdate();

            if (obj.destroys == obj.count && SceneManager.GetActiveScene().name=="Tutorial")
            {
                win.SetActive(true);
                Time.timeScale = 0;
            }

            if (obj.destroys == obj.count)
            {
                letra.SetActive(true);
            }
        }
        else if (col.CompareTag("Letra"))
        {
            letra.SetActive(false);
            


            win.SetActive(true);
            Time.timeScale = 0;
        }
    }




}
