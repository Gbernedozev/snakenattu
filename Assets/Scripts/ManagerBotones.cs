﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerBotones : MonoBehaviour
{
    public GameObject pauseScreen;
    public int nextSceneLoad;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        nextSceneLoad = SceneManager.GetActiveScene().buildIndex+1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame()
    {
        SceneManager.LoadScene("Nivel1", LoadSceneMode.Single);
    }
    public void Tutorial()
    {
        SceneManager.LoadScene("Tutorial", LoadSceneMode.Single);
    }
    public void Niveles()
    {
        SceneManager.LoadScene("Niveles", LoadSceneMode.Single);
    }
    public void Creditos()
    {
        SceneManager.LoadScene("Creditos", LoadSceneMode.Single);
    }
    public void MenuPrincipal()
    {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
    public void Nivel1()
    {
        SceneManager.LoadScene("Nivel1", LoadSceneMode.Single);
    }
    public void Nivel2()
    {
        SceneManager.LoadScene("Nivel2", LoadSceneMode.Single);
    }
    public void Nivel3()
    {
        SceneManager.LoadScene("Nivel3", LoadSceneMode.Single);
    }
    public void Nivel4()
    {
        SceneManager.LoadScene("Nivel4", LoadSceneMode.Single);
    }
    public void Nivel5()
    {
        SceneManager.LoadScene("Nivel5", LoadSceneMode.Single);
    }
    public void Reiniciar()
    {
        Scene scene = SceneManager.GetActiveScene();

        SceneManager.LoadScene(scene.name);
    }
    public void Pausa()
    {
        pauseScreen.SetActive(true);
        Time.timeScale = 0;
        
    }
    public void Continuar()
    {
        pauseScreen.SetActive(false);
        Time.timeScale = 1;
    }

    public void Salir()
    {
        Application.Quit();
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(nextSceneLoad);
        if (nextSceneLoad > PlayerPrefs.GetInt("LevelAt"))
        {
            PlayerPrefs.SetInt("levelAt", nextSceneLoad);
        }
    }

    public void ResetLevels()
    {
        PlayerPrefs.DeleteAll();
        Scene scene = SceneManager.GetActiveScene();

        SceneManager.LoadScene(scene.name);
    }
}
