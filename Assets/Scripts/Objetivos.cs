﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class Objetivos : MonoBehaviour
{
    public int count;
    public int destroys;
    public Text texto;
    // Start is called before the first frame update
    void Start()
    {
        count = transform.childCount;
        destroys = 0;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ObjUpdate()
    {
        texto.text = destroys + "/" + count;
    }
}
