﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveToNextLevel : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        int escena=SceneManager.GetActiveScene().buildIndex;
        if (collision.CompareTag("Player"))
        {

            PlayerPrefs.SetInt("levelAt", escena + 1);
        }
    }
}
