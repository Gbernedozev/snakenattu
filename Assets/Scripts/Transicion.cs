﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transicion : MonoBehaviour
{
    public GameObject Pantalla;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartGame() 
    {
        Pantalla.SetActive(false);
        Time.timeScale = 1;
    }
}
